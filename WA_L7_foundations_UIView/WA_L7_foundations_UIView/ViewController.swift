//
//  ViewController.swift
//  WA_L7_foundations_UIView
//
//  Created by Anatolii on 3/29/19.
//  Copyright © 2019 Anatolii. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var firstNumberField: UITextField!
    @IBOutlet weak var secNumberField: UITextView!
    
    @IBOutlet weak var resultField: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserDefaults()
        
        secNumberField.layer.masksToBounds = true
        secNumberField.layer.borderWidth = 1
        secNumberField.layer.borderColor = UIColor.blue.cgColor
    
    }
    
    @IBAction func addTwoNumbersPressed(_ sender: Any) {
        let firstNumberStr = stringToInt(firstNumberField.text)
        let secNumberStr = stringToInt(secNumberField.text)
        let identifier = "resultString"
        let resultStr = UserDefaults.standard.string(forKey: identifier) ?? ""
        
        if firstNumberStr != nil && secNumberStr != nil {
            let res = firstNumberStr!+secNumberStr!
            resultField.text = String(res)
            let outputText = resultStr + "\(firstNumberStr!) + \(secNumberStr!) = \(res)\n"
            UserDefaults.standard.set(outputText, forKey: identifier)
            secNumberField.text = outputText
            secNumberField.font = UIFont.boldSystemFont(ofSize: CGFloat(res))
        }
    }
    
    func stringToInt( _ str: String?) -> Int? {
        let currInt = Int(str ?? "")
        if currInt == nil {
            let res = "something went wrong"
            secNumberField.text = res
        }
        return currInt
    }
    
    func getUserDefaults() {
        secNumberField.text = UserDefaults.standard.string(forKey: "resultString") ?? ""
    }
}

